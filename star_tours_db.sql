-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 23, 2019 at 08:39 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `star_tours_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE IF NOT EXISTS `bookings` (
  `code` varchar(13) NOT NULL,
  `name` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `passeport` varchar(60) NOT NULL,
  `flightid` int(255) NOT NULL,
  `email` varchar(60) NOT NULL,
  PRIMARY KEY (`code`,`passeport`) USING BTREE,
  KEY `id` (`flightid`) USING BTREE,
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
CREATE TABLE IF NOT EXISTS `flights` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `flightno` varchar(10) NOT NULL,
  `price` int(10) NOT NULL,
  `spaceline` varchar(20) NOT NULL,
  `departure` date NOT NULL,
  `duration` int(11) NOT NULL,
  `origin` varchar(20) NOT NULL,
  `destination` varchar(20) NOT NULL,
  `spacecraft` varchar(60) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `flightno` (`flightno`,`departure`) USING BTREE,
  KEY `spaceline` (`spaceline`),
  KEY `destination` (`destination`),
  KEY `spacecraft` (`spacecraft`),
  KEY `origin` (`origin`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`id`, `flightno`, `price`, `spaceline`, `departure`, `duration`, `origin`, `destination`, `spacecraft`) VALUES
(76, 'BD777', 245, 'Bespin Direct', '2019-07-27', 82, 'Endor', 'Bespin', 'Starspeeder 1000'),
(77, 'EE760', 263, 'Endor Express', '2019-07-27', 61, 'Endor', 'Alderaan', 'Starspeeder 1000'),
(78, 'TT385', 272, 'Tatooine Transit', '2019-07-27', 81, 'Endor', 'Tatooine', 'Starspeeder 3000'),
(79, 'EE328', 251, 'Endor Express', '2019-07-27', 74, 'Bespin', 'Endor', 'Starspeeder 3000'),
(80, 'BD140', 248, 'Bespin Direct', '2019-07-27', 79, 'Bespin', 'Alderaan', 'Starspeeder 1000'),
(81, 'TT389', 265, 'Tatooine Transit', '2019-07-27', 74, 'Bespin', 'Tatooine', 'Starspeeder 3000'),
(82, 'EE930', 279, 'Endor Express', '2019-07-27', 64, 'Alderaan', 'Endor', 'Starspeeder 1000'),
(83, 'BD693', 260, 'Bespin Direct', '2019-07-27', 69, 'Alderaan', 'Bespin', 'Starspeeder 1000'),
(84, 'TT278', 247, 'Tatooine Transit', '2019-07-27', 85, 'Alderaan', 'Tatooine', 'Starspeeder 3000'),
(85, 'TT152', 244, 'Tatooine Transit', '2019-07-27', 85, 'Tatooine', 'Endor', 'Starspeeder 3000'),
(86, 'BD958', 274, 'Bespin Direct', '2019-07-27', 65, 'Tatooine', 'Bespin', 'Starspeeder 1000'),
(87, 'AA846', 280, 'Air Alderaan', '2019-07-27', 61, 'Tatooine', 'Alderaan', 'Starspeeder 1000'),
(88, 'EE714', 241, 'Endor Express', '2019-07-27', 60, 'Endor', 'Bespin', 'Starspeeder 3000'),
(89, 'EE159', 277, 'Endor Express', '2019-07-27', 67, 'Endor', 'Alderaan', 'Starspeeder 1000'),
(90, 'EE460', 279, 'Endor Express', '2019-07-27', 84, 'Endor', 'Tatooine', 'Starspeeder 1000'),
(91, 'BD469', 259, 'Bespin Direct', '2019-07-27', 75, 'Bespin', 'Endor', 'Starspeeder 3000'),
(92, 'AA545', 269, 'Air Alderaan', '2019-07-27', 63, 'Bespin', 'Alderaan', 'Starspeeder 1000'),
(93, 'BD144', 263, 'Bespin Direct', '2019-07-27', 64, 'Bespin', 'Tatooine', 'Starspeeder 3000'),
(94, 'EE838', 274, 'Endor Express', '2019-07-27', 83, 'Alderaan', 'Endor', 'Starspeeder 3000'),
(95, 'AA413', 257, 'Air Alderaan', '2019-07-27', 72, 'Alderaan', 'Bespin', 'Starspeeder 1000'),
(96, 'AA584', 247, 'Air Alderaan', '2019-07-27', 67, 'Alderaan', 'Tatooine', 'Starspeeder 1000'),
(97, 'TT879', 263, 'Tatooine Transit', '2019-07-27', 62, 'Tatooine', 'Endor', 'Starspeeder 3000'),
(98, 'BD682', 272, 'Bespin Direct', '2019-07-27', 89, 'Tatooine', 'Bespin', 'Starspeeder 3000'),
(99, 'TT361', 279, 'Tatooine Transit', '2019-07-27', 69, 'Tatooine', 'Alderaan', 'Starspeeder 3000'),
(100, 'EE151', 250, 'Endor Express', '2019-07-27', 84, 'Endor', 'Bespin', 'Starspeeder 3000'),
(101, 'AA409', 242, 'Air Alderaan', '2019-07-27', 63, 'Endor', 'Alderaan', 'Starspeeder 1000'),
(102, 'TT190', 273, 'Tatooine Transit', '2019-07-27', 74, 'Endor', 'Tatooine', 'Starspeeder 3000'),
(103, 'BD215', 259, 'Bespin Direct', '2019-07-27', 65, 'Bespin', 'Endor', 'Starspeeder 1000'),
(104, 'BD497', 256, 'Bespin Direct', '2019-07-27', 67, 'Bespin', 'Alderaan', 'Starspeeder 1000'),
(105, 'BD707', 253, 'Bespin Direct', '2019-07-27', 78, 'Bespin', 'Tatooine', 'Starspeeder 3000'),
(106, 'AA715', 275, 'Air Alderaan', '2019-07-27', 68, 'Alderaan', 'Endor', 'Starspeeder 1000'),
(107, 'BD803', 259, 'Bespin Direct', '2019-07-27', 80, 'Alderaan', 'Bespin', 'Starspeeder 3000'),
(108, 'TT888', 267, 'Tatooine Transit', '2019-07-27', 76, 'Alderaan', 'Tatooine', 'Starspeeder 3000'),
(109, 'TT917', 262, 'Tatooine Transit', '2019-07-27', 65, 'Tatooine', 'Endor', 'Starspeeder 1000'),
(110, 'TT994', 248, 'Tatooine Transit', '2019-07-27', 70, 'Tatooine', 'Bespin', 'Starspeeder 3000'),
(111, 'AA868', 274, 'Air Alderaan', '2019-07-27', 64, 'Tatooine', 'Alderaan', 'Starspeeder 1000'),
(112, 'EE104', 241, 'Endor Express', '2019-07-27', 76, 'Endor', 'Bespin', 'Starspeeder 3000'),
(113, 'EE655', 263, 'Endor Express', '2019-07-27', 64, 'Endor', 'Alderaan', 'Starspeeder 1000'),
(114, 'TT140', 241, 'Tatooine Transit', '2019-07-27', 82, 'Endor', 'Tatooine', 'Starspeeder 3000'),
(115, 'BD842', 243, 'Bespin Direct', '2019-07-27', 68, 'Bespin', 'Endor', 'Starspeeder 3000'),
(116, 'AA684', 247, 'Air Alderaan', '2019-07-27', 75, 'Bespin', 'Alderaan', 'Starspeeder 3000'),
(117, 'BD716', 273, 'Bespin Direct', '2019-07-27', 90, 'Bespin', 'Tatooine', 'Starspeeder 3000'),
(118, 'EE567', 270, 'Endor Express', '2019-07-27', 76, 'Alderaan', 'Endor', 'Starspeeder 3000'),
(119, 'AA988', 253, 'Air Alderaan', '2019-07-27', 81, 'Alderaan', 'Bespin', 'Starspeeder 1000'),
(120, 'TT488', 253, 'Tatooine Transit', '2019-07-27', 88, 'Alderaan', 'Tatooine', 'Starspeeder 3000'),
(121, 'TT503', 245, 'Tatooine Transit', '2019-07-27', 74, 'Tatooine', 'Endor', 'Starspeeder 1000'),
(122, 'TT987', 250, 'Tatooine Transit', '2019-07-27', 88, 'Tatooine', 'Bespin', 'Starspeeder 3000'),
(123, 'TT378', 260, 'Tatooine Transit', '2019-07-27', 76, 'Tatooine', 'Alderaan', 'Starspeeder 3000'),
(124, 'EE807', 276, 'Endor Express', '2019-07-27', 65, 'Endor', 'Bespin', 'Starspeeder 1000'),
(125, 'AA692', 250, 'Air Alderaan', '2019-07-27', 76, 'Endor', 'Alderaan', 'Starspeeder 3000'),
(127, 'BD399', 262, 'Bespin Direct', '2019-07-27', 69, 'Bespin', 'Endor', 'Starspeeder 3000'),
(128, 'BD337', 244, 'Bespin Direct', '2019-07-27', 81, 'Bespin', 'Alderaan', 'Starspeeder 3000'),
(129, 'TT400', 246, 'Tatooine Transit', '2019-07-27', 63, 'Bespin', 'Tatooine', 'Starspeeder 1000'),
(130, 'EE812', 268, 'Endor Express', '2019-07-27', 79, 'Alderaan', 'Endor', 'Starspeeder 1000'),
(131, 'AA112', 268, 'Air Alderaan', '2019-07-27', 61, 'Alderaan', 'Bespin', 'Starspeeder 3000'),
(132, 'AA960', 268, 'Air Alderaan', '2019-07-27', 72, 'Alderaan', 'Tatooine', 'Starspeeder 1000'),
(133, 'TT985', 259, 'Tatooine Transit', '2019-07-27', 62, 'Tatooine', 'Endor', 'Starspeeder 1000'),
(134, 'BD179', 260, 'Bespin Direct', '2019-07-27', 65, 'Tatooine', 'Bespin', 'Starspeeder 1000'),
(135, 'TT316', 252, 'Tatooine Transit', '2019-07-27', 73, 'Tatooine', 'Alderaan', 'Starspeeder 1000'),
(136, 'EE707', 269, 'Endor Express', '2019-07-27', 75, 'Endor', 'Bespin', 'Starspeeder 3000'),
(137, 'AA904', 241, 'Air Alderaan', '2019-07-27', 89, 'Endor', 'Alderaan', 'Starspeeder 1000'),
(138, 'EE828', 257, 'Endor Express', '2019-07-27', 80, 'Endor', 'Tatooine', 'Starspeeder 3000'),
(139, 'EE968', 246, 'Endor Express', '2019-07-27', 84, 'Bespin', 'Endor', 'Starspeeder 1000'),
(140, 'AA123', 245, 'Air Alderaan', '2019-07-27', 90, 'Bespin', 'Alderaan', 'Starspeeder 3000'),
(141, 'TT245', 246, 'Tatooine Transit', '2019-07-27', 77, 'Bespin', 'Tatooine', 'Starspeeder 1000'),
(142, 'EE687', 253, 'Endor Express', '2019-07-27', 78, 'Alderaan', 'Endor', 'Starspeeder 3000'),
(143, 'BD659', 272, 'Bespin Direct', '2019-07-27', 60, 'Alderaan', 'Bespin', 'Starspeeder 3000'),
(144, 'AA855', 267, 'Air Alderaan', '2019-07-27', 88, 'Alderaan', 'Tatooine', 'Starspeeder 3000'),
(145, 'TT167', 241, 'Tatooine Transit', '2019-07-27', 80, 'Tatooine', 'Endor', 'Starspeeder 1000'),
(146, 'TT137', 251, 'Tatooine Transit', '2019-07-27', 78, 'Tatooine', 'Bespin', 'Starspeeder 1000'),
(147, 'TT679', 255, 'Tatooine Transit', '2019-07-27', 79, 'Tatooine', 'Alderaan', 'Starspeeder 3000');

-- --------------------------------------------------------

--
-- Table structure for table `planet`
--

DROP TABLE IF EXISTS `planet`;
CREATE TABLE IF NOT EXISTS `planet` (
  `name` varchar(20) NOT NULL,
  `image` longtext NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `planet`
--

INSERT INTO `planet` (`name`, `image`) VALUES
('Alderaan', ''),
('Bespin', ''),
('Endor', ''),
('Tatooine', '');

-- --------------------------------------------------------

--
-- Table structure for table `spacecraft`
--

DROP TABLE IF EXISTS `spacecraft`;
CREATE TABLE IF NOT EXISTS `spacecraft` (
  `name` varchar(60) NOT NULL,
  `designer` varchar(60) NOT NULL,
  `class` varchar(60) NOT NULL,
  `crew` int(11) NOT NULL,
  `passengers` int(11) NOT NULL,
  `image` longtext,
  PRIMARY KEY (`name`),
  KEY `name` (`name`),
  KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacecraft`
--

INSERT INTO `spacecraft` (`name`, `designer`, `class`, `crew`, `passengers`, `image`) VALUES
('Starspeeder 1000', 'Sacul Aerospace', 'Spaceliner', 3, 40, NULL),
('Starspeeder 3000', 'Sacul Aerospace', 'Spaceliner', 2, 40, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `spacelines`
--

DROP TABLE IF EXISTS `spacelines`;
CREATE TABLE IF NOT EXISTS `spacelines` (
  `name` varchar(20) NOT NULL,
  `image` longtext,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spacelines`
--

INSERT INTO `spacelines` (`name`, `image`) VALUES
('Air Alderaan', ''),
('Bespin Direct', ''),
('Endor Express', ''),
('Tatooine Transit', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `email` varchar(60) NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `password`) VALUES
('demo@hotmail.com', '5653e1a36d11e4b0f8698cb8dd3fb8e230e128ec9f8a770ab19420a782c3a52a'),
('mrdylanas@hotmail.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
('tyui@hotmail.com', 'd185e0859cf3c74231741392ff8eb8b3ff3e508dce28b69cf4747f30dac7368d');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`flightid`) REFERENCES `flights` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `email` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Constraints for table `flights`
--
ALTER TABLE `flights`
  ADD CONSTRAINT `flights_ibfk_1` FOREIGN KEY (`spacecraft`) REFERENCES `spacecraft` (`name`),
  ADD CONSTRAINT `flights_ibfk_2` FOREIGN KEY (`origin`) REFERENCES `planet` (`name`),
  ADD CONSTRAINT `flights_ibfk_3` FOREIGN KEY (`destination`) REFERENCES `planet` (`name`),
  ADD CONSTRAINT `flights_ibfk_4` FOREIGN KEY (`spaceline`) REFERENCES `spacelines` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
