<?php
session_start();
    require('conn.php');

    //debug($_POST);

    if (isset($_POST['id']) && isset($_POST['passenger'])){
        
        $id = $_POST['id'];
        $passengers = $_POST['passenger'];
        
        $flight = get_flight($pdo, $id);
        if ($flight == null || sizeof($passengers) == 0){
            header('Location: bookingform.php');
            exit();
        }
        $flight = $flight[0];
        
        $email = $_SESSION["email"];
        $code = book_flight($pdo, $id, $passengers, $email);
        header('Location: thankyou.php?&code='.$code);
        exit();
    } else {
        header('Location: bookingform.php');
        exit();
    }
