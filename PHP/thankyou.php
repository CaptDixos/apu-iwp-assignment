<?php
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
    require('conn.php');

    if (isset($_GET['code'])){
        $code = $_GET['code'];
        $books = get_book($pdo, $code);
        $flight = get_flight($pdo, $books[0]['flightid']);
        $flight = $flight[0];
    } else {
        header('Location: bookingform.php');
        exit();
    }

?>

<html>

<head>
    <title>
        Star Tours - Thank you
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/thankyou.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="thankyou">
            <h1>Thank you</h1><br>
            <h2 class="encadre">A seat has been reserved for every passenger in this flight.<br>You have 72h to confirm your trip.</h2><br>


            <div class="recap">
                <h1>You have chosen : </h1><br>
                <table class="trecap" style="width:100%">
                    <tr>
                        <th>Price</th>
                        <th>Spaceline</th>
                        <th>Flight number</th>
                        <th>Depart</th>
                        <th>Duration</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Spacecraft</th>
                    </tr>
                    <tr>
                        <td><?php echo $flight['price']; ?></td>
                        <td><?php echo $flight['spaceline']; ?></td>
                        <td><?php echo $flight['flightno']; ?></td>
                        <td><?php echo $flight['departure']; ?></td>
                        <td><?php echo $flight['duration']; ?></td>
                        <td><?php echo $flight['origin']; ?></td>
                        <td><?php echo $flight['destination']; ?></td>
                        <td><?php echo $flight['spacecraft']; ?></td>
                    </tr>
                </table>
            </div>
            <div class="passengerdetails">
                <h1>Passenger details : </h1><br>
                <table class="pdetails" style="width:100%">
                    <tr>
                        <th>Name</th>
                        <th>Last Name</th>
                        <th>Passport number</th>
                    </tr>
                    <?php foreach($books as $book){ ?>
                    <tr>
                        <td><?php echo $book['name']; ?></td>
                        <td><?php echo $book['lastname']; ?></td>
                        <td><?php echo $book['passeport']; ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>

            <h3 class="yellow">How to buy this ticket</h3>
            <p>Tickets cannot be purchased directly from Star Tours Software.<br>
                Provide this information to a travel agent to help them match the fares found.<br><br>
            </p>
            <div class="encadre">
                <p>Make sure to provide the exact booking code shown :
                </p>
                <h2 class="yellow"><?php echo $book['code']; ?></h2>
            </div>
            <h1>Thank you for flying with star tours</h1>
            <h1 class="yellow">May the force be with you</h1>
            <div class="btn-group">
                <a href="index.php" class="btn"><i class="fa fa-home"></i> Go home</a>
            </div>
        </div>
    </div>
</body>

</html>
