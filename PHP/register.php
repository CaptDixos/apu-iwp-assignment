<?php
session_start();
$fail = false;
if(isset ($_SESSION["email"])){
    header('Location: index.php');
    exit();
}
if(isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["repeatpassword"])){
    if($_POST["password"] == $_POST["repeatpassword"])
    {
        require('conn.php');
        if(register($pdo, $_POST["email"], $_POST["password"])){ // if register() == true;

            $_SESSION["email"]=$_POST["email"];
            header('Location: index.php?welcome');
            exit();
        }
    }
    
        $fail = true;
    
}

?>

<!--doctype html-->
<html>
<head>
    <title>
        Star Tours - Register
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/register.css"> <!-- linking the css -->
</head>
    <body>
        <div class="font">
        <div class="sign-up-form">
            <img src="images/index_logo.png">
            <h1>Sign up now</h1>
            <?php if($fail) {
                echo 'Register fail, please retry';
            }
            ?>
            <form method="post" action="register.php">
            <input name="email" type="email" class="input-box" placeholder="Your email">
            <input name="password" type="password" class="input-box" placeholder="Your password">
            <input name="repeatpassword" type="password" class="input-box" placeholder="Repeat password">
            <button type="submit" class="signup-btn">Sign up</button>
            <p>Do you have an account ? <a class="yellow" href="login.php">Sign in</a></p>
            <br><br><a href="index.php" class="btn">Home</a>
            </form>
        </div>
        </div>
    </body>
</html>