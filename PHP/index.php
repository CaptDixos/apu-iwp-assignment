<?php
session_start();
$fail = false;
if(isset($_POST["email"]) && isset($_POST["password"])){
    require('conn.php');
    
    if(login($pdo, $_POST["email"], $_POST["password"])){ // if login() == true;
        
        $_SESSION["email"]=$_POST["email"];
        header('Location: index.php?welcome');
        exit();
    }
    
    $fail = true;
}

?>

<!--doctype html-->
<html>

<head>
    <title>
        Star Tours - Book a flight, browse our flights offers and explore the Star Tours Experience
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/index.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> <!-- linking the Font Awesome from Bootstrap CDN (for example for icons)-->
</head>

<body>
    <div class="font">
        <section class="header">
            <div class="container">
                <img src="images/index_logo.png">
                <a href="enterbookingcode.php" class="managebooking">See my bookings</a>
                <?php if(isset($_SESSION["email"])){ ?>
                <a href="logout.php" type="button" class="register-btn">Logout</a>
                <?php } else { ?>
                <a href="login.php" type="button" class="login-btn">Login</a>
                <a href="register.php" type="button" class="register-btn">Register</a>
                <?php } ?>
                
    
            </div>
            <h1>Travel across the Stars</h1>
            <p>Welcome to the Star Tours website</p>
            <div>
                <a href="bookingform.php" class="searchflightbutton"><i class="fa fa-space-shuttle">
                    </i> Search flights</a>
            </div>
            <div class="btn-group">
                <a href="index.php" class="btn"><i class="fa fa-home">
                    </i> Home</a>
                <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                    </i> our fleet</a>
                <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                    </i> Safety procedures</a>
                <a href="about.php" class="btn"><i class="fa fa-rebel">
                    </i> our team</a>
            </div>
        </section>
        <section class="features">
            <h1>Featured Destinations</h1>
            <div class="container">
                <div class="row">
                    <div class="column">
                        <div class="feature-box">
                            <div class="feature-img">
                                <img src="images/index_image1.jpg">

                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="feature-details">
                            <h4>Tatooine</h4>
                            <p>Join us on the Trek to Tatooine. Start your visit with a trip to the Galactic Zoo. Then race over to the Mos Eisley Cantina, for cocktails with the galaxy’s most outrageous characters. If adventure is your middle name, this is the tour for you: The Trek to Tatooine! Reservations are limited, so call your travel agent or Star Tours today.</p>
                            <div>
                                <span><i class="fa fa-map-marker"></i>Tatooine</span>
                                <span><i class="fa fa-sun-o"> 4 days</i></span>
                                <span><i class="fa fa-moon-o"> 3 nights</i></span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="feature-box">
                            <div class="feature-img">
                                <img src="images/index_image2.png">

                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="feature-details">
                            <h4>Hoth</h4>
                            <p>Hoth is the planet where you can ski the most incredible slopes in the galaxy. Or if you prefer, explore beautiful and mysterious ice caverns and the famed Echo Base of the Rebellion forces. And while you’re there, be sure to enjoy an exhilarating ride on a Tauntaun. It’s all on Hoth, and it all begins soon, only from Star Tours. </p>
                            <div>
                                <span><i class="fa fa-map-marker"></i>Hoth</span>
                                <span><i class="fa fa-sun-o"> 3 days</i></span>
                                <span><i class="fa fa-moon-o"> 2 nights</i></span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="feature-box">
                            <div class="feature-img">
                                <img src="images/index_image3.jpg">

                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="feature-details">
                            <h4>Endor</h4>
                            <p>Star Tours is now offering convenient daily departures to the exotic Moon of Endor. Come spend an afternoon or the entire day with the lovable Ewoks, in their charming tribal villages. It’s a fun-filled visit that you and your family will remember forever! Just ask for the Endor Express. Available only from Star Tours. Visit Endor today. </p>
                            <div>
                                <span><i class="fa fa-map-marker"></i>Endor</span>
                                <span><i class="fa fa-sun-o"> 2 days</i></span>
                                <span><i class="fa fa-moon-o"> 1 night</i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="banner">
            <div class="banner-highlights">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- big column -->
                            <h2>Get 25% discount on featured destinations</h2>
                            <p>Book your tickets before June 30th</p>
                        </div>
                        <div class="col-md-4">
                            <!-- small column -->
                            <br>
                            <a href="bookingform.php" class="booking-btn">. Book Now .</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="videosection">
            <br><br>
            <h1>Enjoy our Star Tours Gateway Package : </h1>
            <video class="video" width="720" height="405" controls poster="video/startours.jpg">
                <source src="video/startours.mp4" type="video/mp4">
                <source src="video/startours.ogg" type="video/ogg">
                <source src="video/startours.webm" type="video/webm">
                Your browser does not support the video tag or the file format of this video.
            </video>
        </section>
        <section class="users-feedback">
            <h1>Users Reviews</h1>
            <div class="container">
                <div class="row">
                    <div class="columnr">
                        <div class="user-review">
                            <p>" Star Tours lived up to its incredible reputation. The cabin crew were fun to interact with throughout the flight. The in-flight entertainment had quite a selection of current movies. The cabin crew made us feel like we were on our own private ship. I'd highly recommend Star Tours, based on this experience. "</p>
                            <h5>Yoda</h5>
                            <small>Dagobah</small>
                        </div>
                        <img src="images/index_ufeedback1.png">
                    </div>
                    <div class="columnr">
                        <div class="user-review">
                            <p>" Outbound flight delayed with one hour due to comets around Endor, so not the fault of Star Tours. Inbound flight on time departure and arrival. Cabin crew on both flights friendly. New spacecraft on both flights with new seats. I would fly them without hesitation when price and hours are acceptable. "</p>
                            <h5>Stormtrooper TK-421</h5>
                            <small>Endor</small>
                        </div>
                        <img src="images/index_ufeedback2.png">
                    </div>
                    <div class="columnr">
                        <div class="user-review">
                            <p>" Best travel agency to travel especially for long haul flights. Good entertainment, good hospitality, nice selection of food, drinks and snacks. The Crew were very polite and full of smiles. I have not me such dedicated and hard-working attendants on any other spaceline. I will definitely fly with Star Tours again ! "</p>
                            <h5>Jar Jar</h5>
                            <small>Naboo</small>
                        </div>
                        <img src="images/index_ufeedback3.png">
                    </div>
                </div>
            </div>
        </section>
        <section class="footer">
            <div class="row">
                <div class="column3">

                </div>
                <div class="column3">
                    <img src="images/index_logo.png" class="footer-logo">
                </div>
                <div class="column3">
                    <h4>Quick Contact</h4>
                    <p><i class="fa fa-phone-square"></i> +45 215 368 4177</p>
                    <p><i class="fa fa-envelope-square"></i> contact@startours.sw</p>
                    <p><i class="fa fa-home"></i> 33.8118°N 117.9177°W</p>
                </div>
                <div class="column3">
                    <h4>Follow Us</h4>
                    <p><i class="fa fa-facebook-official"></i> Starbook</p>
                    <p><i class="fa fa-youtube-play"></i> YouStar</p>
                    <p><i class="fa fa-twitter"></i> TwitStar</p>
                </div>
            </div>
            <hr> <!-- horizontal line -->
            <p class="copyright">Made with <i class="fa fa-heart"></i> by Dylan ASMAR</p>
        </section>
    </div>
</body>

</html>
