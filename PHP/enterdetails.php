<?php
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
    require('conn.php');
    if (isset($_GET['id'])){
        $id = $_GET['id'];
        $flight = get_flight($pdo, $id);
        if ($flight == null){
            header('Location: bookingform.php');
            exit();
        }
        $flight = $flight[0];
        
        $passengers = 1;
        if (isset($_GET['passengers'])){
            $passengers = intval($_GET['passengers']);
        }
        
    } else {
        header('Location: bookingform.php');
        exit();
    }
?>
<html>

<head>
    <title>
        Star Tours - Enter passenger details
    </title>
    <link rel="stylesheet" href="css/enterdetails.css"> <!-- linking the css -->
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <h1>Please enter passenger details</h1>
        <form class="form" action="enterdetails.php" method="GET">
            <label>Passengers: </label><input class="input-box" type="number" name="passengers" value="<?php echo $passengers; ?>">
            <input class="submit" type="submit" value="Submit">
            <input type="hidden" name="id" value="<?php echo $flight['id']; ?>">
        </form>
        <form class="form" action="book.php" method="post">
            <?php for($i = 1; $i <= $passengers; $i++) { ?>
            <div>Passenger <?php echo $i; ?></div>
            <input required class="input-box" type="text" name="passenger[<?php echo $i; ?>][name]" placeholder="First Name">
            <input required class="input-box" type="text" name="passenger[<?php echo $i; ?>][lastname]" placeholder="Last Name">
            <input required class="input-box" type="text" name="passenger[<?php echo $i; ?>][passeport]" placeholder="Passeport No"><br><br>
            <?php } ?>
            <input type="hidden" name="id" value="<?php echo $flight['id']; ?>">
            <input type="submit" class="submit" value="Submit">
        </form>
    </div>
</body>

</html>
