<?php
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
    require('conn.php');
    //debug($_GET);

    if (isset($_GET['origin']) && isset($_GET['destination']) && isset($_GET['date'])){
        
        $origin = $_GET['origin'];
        $destination = $_GET['destination'];
        $date = $_GET['date'];
        $flights = get_flights($pdo, $origin, $destination, $date);
    } else {
        header('Location: bookingform.php');
        exit();
    }
?>
<html>

<head>
    <title>
        Star Tours - Flight Results
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/flightresults.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Home</a>
            <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                </i> our fleet</a>
            <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                </i> Safety procedures</a>
            <a href="about.php" class="btn"><i class="fa fa-rebel">
                </i> our team</a>
        </div>
    </div>


    <h1 class="h1">Flight results</h1>
    <?php if($flights != null){ ?>
    <table class="flightresults" style="width:100%">
        <tr>
            <th>Price</th>
            <th>Spaceline</th>
            <th>Flight number</th>
            <th>Depart</th>
            <th>Duration</th>
            <th>From</th>
            <th>To</th>
            <th>Spacecraft</th>
            <th></th>
        </tr>
        <?php foreach($flights as $flight){ ?>
        <tr>
            <td><?php echo $flight['price']; ?></td>
            <td><?php echo $flight['spaceline']; ?></td>
            <td><?php echo $flight['flightno']; ?></td>
            <td><?php echo $flight['departure']; ?></td>
            <td><?php echo $flight['duration']; ?></td>
            <td><?php echo $flight['origin']; ?></td>
            <td><?php echo $flight['destination']; ?></td>
            <td><?php echo $flight['spacecraft']; ?></td>
            <td><a href="flightconfirmation.php?&id=<?php echo $flight['id']; ?>" class="btnblue">Book Now</a></td>
        </tr>
        <?php } ?>
    </table>
    <?php } else { ?>
    <h1 class="else">No flights found.</h1>
    <?php } ?>

</body>

</html>
