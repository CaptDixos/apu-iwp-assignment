<?php
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
    require('conn.php');
    if (isset($_GET['id'])){
        $id = $_GET['id'];
        $flight = get_flight($pdo, $id);
        if ($flight == null){
            header('Location: bookingform.php');
            exit();
        }
        
        $flight = $flight[0];
    } else {
        header('Location: bookingform.php');
        exit();
    }

?>

<html>

<head>
    <title>
        Star Tours - Flight Confirmation
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/flightconfirmation.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Back Home</a><br><br>
        </div>
        <div class="confirmation">
            <h1>You have chosen : </h1><br>
            <table class="flightconfirmation" style="width:100%">
                <tr>
                    <th>Price</th>
                    <th>Spaceline</th>
                    <th>Flight number</th>
                    <th>Depart</th>
                    <th>Duration</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Spacecraft</th>
                </tr>
                <tr>
                    <td><?php echo $flight['price']; ?></td>
                    <td><?php echo $flight['spaceline']; ?></td>
                    <td><?php echo $flight['flightno']; ?></td>
                    <td><?php echo $flight['departure']; ?></td>
                    <td><?php echo $flight['duration']; ?></td>
                    <td><?php echo $flight['origin']; ?></td>
                    <td><?php echo $flight['destination']; ?></td>
                    <td><?php echo $flight['spacecraft']; ?></td>
                </tr>
            </table>
            <h1>Do you confirm ?</h1>
            <a href="enterdetails.php?&id=<?php echo $flight['id']; ?>" class="yesno1"><i class="fa fa-check"></i> Yes, and may the force be with us</a>
            <br><br>
            <a href="index.php" class="yesno2"><i class="fa fa-times"></i> No, go home</a>
        </div>
    </div>
</body>

</html>
