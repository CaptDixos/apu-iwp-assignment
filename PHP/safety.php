<!--doctype html-->
<html>

<head>
    <title>
        Star Tours - Safety
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/safety.css"> <!-- linking the css -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Home</a>
            <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                </i> our fleet</a>
            <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                </i> Safety procedures</a>
            <a href="about.php" class="btn"><i class="fa fa-rebel">
                </i> our team</a>
        </div>
        <h2>Safety procedures</h2>
        <button class="dropdown-toggle" data-toggle="dropdown-1">
            Droids
        </button><br>
        <div class="dropdown dropdown-1">
            All droids leaving the system must be cleared by Customs Control. Proof of ownership is required for all droid passengers.
        </div><br>

        <button class="dropdown-toggle" data-toggle="dropdown-2">
            Interplanetary travelers
        </button><br>
        <div class="dropdown dropdown-2">
            All interplanetary travelers must have a current passport and the necessary visas prior to leaving the space port. Passengers requiring assistance should visit the nearest information kiosk. Thank you.
        </div><br>


        <button class="dropdown-toggle" data-toggle="dropdown-3">
            Safety instructions
        </button><br>
        <div class="dropdown dropdown-3">
            When the automatic doors have opened, please proceed directly across the ramp into the cabin. Continue to move all the way across your aisle, filling in every available seat. For your safety, all passengers are required to wear safety restraints throughout the flight. To fasten the restraint, pull the strap out from the right side of the seat and snap it into the console on your left. Galactic regulations require that all carry-on items be safely stowed beneath your seat. While onboard, flash photography is not permitted.
        </div><br>

        <button class="dropdown-toggle" data-toggle="dropdown-4">
            Hand baggage requirements
        </button><br>
        <div class="dropdown dropdown-4">
            - Each liquid must be in its own container of no more than 100ml (3.4oz). <br>
            - You need to put all items in a single, transparent, re-sealable plastic bag of up to 20 x 20cm (8 x 8in) with a total capacity of up to one litre (approx. one quart). <br>
            - You must be able to completely close the bag and fit it in your hand baggage. <br>
            - You must take the bag of liquids out of your hand baggage to be screened separately.
        </div><br>

        <button class="dropdown-toggle" data-toggle="dropdown-5">
            Toiletries, medecines and aerosols
        </button><br>

        <div class="dropdown dropdown-5">
            - You can take non-radioactive medicines or toiletries (incl. aerosols), such as hair sprays, perfumes, colognes and medicines containing alcohol in your hand or checked baggage. <br>
            - You can take non-flammable, non-toxic, non-corrosive aerosols for sporting or home use in your checked baggage only.
        </div><br>

        <button class="dropdown-toggle" data-toggle="dropdown-6">
            Batteries
        </button><br>
        <div class="dropdown dropdown-6">
            You can take with you for your own personal use, portable electronic devices that contain lithium batteries such as a laptop, tablet, smart phone, camera and music player.

            Please always ensure that you:<br>

            - Pack all battery-powered devices to prevent accidental activation. <br>
            - Protect spare batteries from short circuit and damage by keeping them in their original packaging (if possible), in a protective case or a strong plastic bag, or by placing electrical tape over the terminals. <br>
            - Don't take any damaged batteries or equipment with you. <br>
        </div><br>
        <script>
            $('.dropdown-toggle').click(function() {
                var target = $(this).data("toggle");
                $("." + target).stop().slideToggle(200);
            });

        </script>
    </div>
</body>

</html>
