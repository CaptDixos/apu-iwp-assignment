<?php
session_start();
$fail = false;
if(isset ($_SESSION["email"])){
    header('Location: index.php');
    exit();
}
if(isset($_POST["email"]) && isset($_POST["password"])){
    require('conn.php');
    
    if(login($pdo, $_POST["email"], $_POST["password"])){ // if login() == true;
        
        $_SESSION["email"]=$_POST["email"];
        header('Location: index.php?welcome');
        exit();
    }
    
    $fail = true;
}

?>


<!--doctype html-->
<html>

<head>
    <title>
        Star Tours - Login
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/login.css"> <!-- linking the css -->
</head>

<body>
    <div class="font">
        <div class="login-form">
            <img src="images/index_logo.png">
            <h1>Login now</h1>
            <?php if($fail) {
                echo 'Login fail, please retry';
            }
            ?>
            <form method="post" action="login.php">
                <input name="email" type="email" class="input-box" placeholder="Your email">
                <input name="password" type="password" class="input-box" placeholder="Your password">
                <button type="submit" class="login-btn">Login</button>
                <p>Don't have an account ? <br> <a class="yellow" href="register.php">Sign up</a></p>
                <br><br><a href="index.php" class="btn">Home</a>
            </form>
        </div>
    </div>
</body>

</html>
