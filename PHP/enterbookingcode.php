<?php 
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
require('conn.php');
$code='';
$email = $_SESSION["email"];
$bookings = getbookings($pdo, $email);
?>

<html>

<head>
    <title>
        Star Tours - Enter booking code
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/enterbookingcode.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="enterbookingcode">
            <form class="form" action="seemybooking.php" method="GET">
                <label>
                    <h1 class="yellow">Available booking codes </h1>
                </label><br>
                <?php foreach($bookings as $booking){ ?>
                <a href="seemybooking.php?code=<?php echo $booking['code']; ?>" type="button" class="btnyellow"><?php echo $booking['code']; ?></a>
                <br><br><?php } ?>
                <br><br><a href="index.php" class="btn">go home</a>
            </form>
        </div>
    </div>
</body>
</html>