<?php
session_start();
if(!isset($_SESSION["email"])){
    header('Location: login.php');
    exit();
}
    require('conn.php');
    $planets = get_planets($pdo);

    //debug($planets);
?>

<html>

<head>
    <title>
        Star Tours - Booking form
    </title>
    <link rel="stylesheet" href="css/bookingform.css"> <!-- linking the css -->
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Home</a>
            <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                </i> our fleet</a>
            <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                </i> Safety procedures</a>
            <a href="about.php" class="btn"><i class="fa fa-rebel">
                </i> our team</a>
        </div>
        <div class="booking-form-box">
            <h3>One Way only, Round Trip available soon</h3>
            <form method="GET" action="flightresults.php">
                <div class="booking-form">
                    <label>Flying from</label>
                    <select name="origin" class="form-control" placeholder="City or Astroport"><br><br>
                        <?php
                    foreach($planets as $planet){ ?>
                        <option value="<?php echo $planet['name']; ?>">
                            <?php echo $planet['name']; ?>
                        </option>
                        <?php
                    }
                ?>
                    </select>
                    <label>Flying to</label>
                    <select name="destination" class="form-control" placeholder="City or Astroport">
                        <?php
                    foreach($planets as $planet){ ?>
                        <option value="<?php echo $planet['name']; ?>">
                            <?php echo $planet['name']; ?>
                        </option>
                        <?php
                    }
                ?>
                    </select>
                    <div class="input-grp">
                        <label>Date of departure</label>
                        <input type="date" name="date" required class="form-control-g select-date" value ="2019-07-27">
                        <input type="submit" class="showflights" value="Show flights">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
