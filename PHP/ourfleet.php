<!--doctype html-->
<html>

<head>
    <title>
        Star Tours - Our fleet
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/ourfleet.css"> <!-- linking the css -->
    <link href="http://allfont.net/allfont.css?fonts=star-jedi" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Home</a>
            <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                </i> our fleet</a>
            <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                </i> Safety procedures</a>
            <a href="about.php" class="btn"><i class="fa fa-rebel">
                </i> our team</a>
        </div>
        <section class="ourfleet">
            <br>
            <h1>Our fleet</h1><br>
        </section>
        <section>
            <div class="row">
                <div class="column1">
                    <img src="images/ourfleet_Starspeeder1000.jpg">
                    <h3>26 StarSpeeder 1000</h3>
                    <p>Designer : Sacul Aerospace </p>
                    <p>Class : Spaceliner </p>
                    <p>Hyperdrive system : T-14 </p>
                    <p>Crew : 3 </p>
                    <p>Passengers : 40 </p>
                    <p>Armament : 2 flash cannons, 2 laser cannons</p>
                </div>
                <div class="column2">
                    <img src="images/ourfleet_Starspeeder3000.jpg">
                    <h3>18 StarSpeeder 3000</h3>
                    <p>Designer : Sacul Aerospace </p>
                    <p>Class : Spaceliner </p>
                    <p>Hyperdrive system : RX </p>
                    <p>Crew : 2 </p>
                    <p>Passengers : 40</p>
                    <p>Armament : 2 laser cannons</p>
                </div>
            </div>
        </section>
        <section class="fcabobop">
            <br><br>
            <h1>Flights sold by us are operated by : </h1><br><br>
            <div class="row">
                <div class="column1">
                    <img src="images/ourfleet_AirAlderaan.png">
                    <h2>Air Alderaan</h2>
                </div>
                <div class="column2">
                    <img src="images/ourfleet_BespinDirect.png">
                    <h2>Bespin Direct</h2>
                </div>
                <div class="column1">
                    <img src="images/ourfleet_NabooSpacelines.png">
                    <h2>Naboo Spacelines</h2>
                </div>
                <div class="column2">
                    <img src="images/ourfleet_TatooineTransit.png">
                    <h2>Tatooine Transit</h2>
                </div>
            </div>
        </section>
    </div>
</body>

</html>
