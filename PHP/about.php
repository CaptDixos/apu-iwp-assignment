<!-- a propos + notre histoire -->

<!--doctype html-->
<html>

<head>
    <title>
        Star Tours - About us
    </title>
    <link rel="shortcut icon" href="https://i.ibb.co/1fnRbmV/logo.png">
    <link rel="stylesheet" href="css/about.css"> <!-- linking the css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="font">
        <img src="images/index_logo.png">
        <div class="btn-group">
            <a href="index.php" class="btn"><i class="fa fa-home">
                </i> Home</a>
            <a href="ourfleet.php" class="btn"><i class="fa fa-info-circle">
                </i> our fleet</a>
            <a href="safety.php" class="btn"><i class="fa fa-exclamation-circle">
                </i> Safety procedures</a>
            <a href="about.php" class="btn"><i class="fa fa-rebel">
                </i> our team</a>
        </div>
        <div class="team-section">
            <h1>team</h1>
            <div class="staff">
                <a href="#"><img src="images/about_image1.jpg" alt=""></a>
                <a href="#"><img src="images/about_image2.jpg" alt=""></a>
                <a href="#"><img src="images/about_image3.jpg" alt=""></a>
            </div>
        </div>
        <div class="section">
            <span class="border"></span>
            <h1>What is Star Tours ?</h1>
            <p>Star Tours, also known as Star Tours Spacelines, was an interstellar travel agency which gave tours to prominent planets and moons in the galaxy. </p>
            <span class="border"></span>
            <h1>Fleet</h1>
            <p>Star Tours's fleet was once dominated by the 40-passenger StarSpeeder 1000 piloted by AC series pilot droids, but now, the carrier is transitioning to a next-generation fleet of StarSpeeder 3000 vehicles, such as Transport 22. These vessels are piloted by competent RX-Series pilot droids and carry an astromech unit.</p>
            <span class="border"></span>
            <h1>Destinations</h1>
            <p>Abregado-rae, Alderaan, Behpour, Berchest, Bespin, Bimmisaari, Bogden, Carosi XII, Cato Neimoidia, Chandrila, Christophsis, Corellia, Coruscant, Dac, Dagobah, Dantooine, Dathomir, Elshandruu Pica, Empress Teta, Endor, Etti, Felucia, Garos IV, Geonosis, Glee Anselm, Hoth, Ithor, Kalarba, Kamino, Kashyyyk, Kway Teow, Mandalore, Malastare, Metalorn, Muunilinst, Mustafar, Mygeeto, Naboo, Nubia, Ord Cantrell, Ord Mantell, Panna, Polis Massa, Praya, Rafa V, Ralltiir, Rhen Var, Rodia, Ryloth, Shili, Shumari, Skako, Storinal, Tatooine, Telerath, Teyr, Trandosha, Umgul, Utapau, Xagobah, Yavin 4.</p>
            <span class="border"></span>
            <h1>Vacation Packages</h1>
            <div class="packages">
                <p>Deluxe Undersea Package</p>
                <p>Endor Express</p>
                <p>Family Adventure Package</p>
                <p>Frontier Vacation Package</p>
                <p>Moonlight Cruise (Star Tours)</p>
                <p>Star Tours Getaway Package</p>
                <p>Trek to Tatooine</p>
            </div>
        </div>
    </div>
</body>

</html>
