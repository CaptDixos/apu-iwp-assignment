<?php
$HOST='127.0.0.1';
$PORT='3306';
$USER='root';
$PASSWORD='';
$DATABASE='star_tours_db';

$dsn = "mysql:host=$HOST;dbname=$DATABASE;charset=utf8mb4";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $USER, $PASSWORD, $options);
} catch (PDOException $e){
    exit('Database error. ' . $e->getMessage());
}

function debug($msg){
    echo '<pre>';
    print_r($msg);
    echo '</pre>';
}

function get_planets($pdo){
    $planets = null;
    
    $sql = "SELECT name FROM planet";
    
    // requete preparee (eviter les injections SQL)
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    if ($stmt->rowCount() > 0){ //si stmt est bon
        $planets = $stmt->fetchAll(); // $planets est un tableau avec toutes les lignes de la réponse
    }
    
    return $planets;
}

function get_flights($pdo, $origin, $destination, $date){
    $flights = null;
    
    $sql = "SELECT * FROM flights WHERE origin = ? AND destination = ? AND departure = ?";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$origin, $destination, $date]);
    
    if ($stmt->rowCount() > 0){
        $flights = $stmt->fetchAll();
    }
    
    return $flights;
}

function get_flight($pdo, $id){
    $flight = null;
    
    $sql = "SELECT * FROM flights WHERE id = ?";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$id]);
    
    if ($stmt->rowCount() > 0){
        $flight = $stmt->fetchAll();
    }
    
    return $flight;
    
}

function book_flight($pdo, $id, $passengers, $email){
    // Generer le code
    $code = uniqid();
    try {
        // Insérer les passagers
        foreach($passengers as $passenger){
            $name = $passenger['name'];
            $lastname = $passenger['lastname'];
            $passeport = $passenger['passeport'];

            // Execution SQL
            $sql = "INSERT INTO `bookings` (`code`, `name`, `lastname`, `passeport`, `flightid`, email) VALUES (?, ?, ?, ?, ?, ?);";

            $stmt = $pdo->prepare($sql);
            $stmt->execute([$code, $name, $lastname, $passeport, $id, $email]);
        }
    } catch (PDOException $e){
        exit($e->getMessage());
    }
    
    return $code;
}

function get_book($pdo, $code) {
    $book = null;
    
    $sql = "SELECT * FROM bookings WHERE code = ?";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$code]);
    
    if ($stmt->rowCount() > 0){
        $book = $stmt->fetchAll();
    }
    
    return $book;
}

function login($pdo, $email, $password) {
    $password = hash("sha256", $password);
    
    $sql = "SELECT * FROM users WHERE email = ? and password = ?";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$email, $password]);
    
    if ($stmt->rowCount() > 0){
        return true;
    } else {
        return false;
    }
}

function register($pdo, $email, $password){
    
    try {
        $password = hash("sha256", $password);
        // Execution SQL
        $sql = "INSERT INTO `users` (`email`, `password`) VALUES (?, ?);";

        $stmt = $pdo->prepare($sql);
        $stmt->execute([$email, $password]);
    } catch (PDOException $e){
        return false;
    }
    
    return true;
}

function getbookings($pdo, $email){
    $booking = null;
    
    $sql = "SELECT distinct code FROM bookings WHERE email = ?";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$email]);
    
    if ($stmt->rowCount() > 0){
        $booking = $stmt->fetchAll();
    }
    
    return $booking;
}

function create_flights($pdo, $pricebase=260, $generation=6){
    
    $spacelines = [
        'Endor' => 'Endor Express',
        'Bespin' => 'Bespin Direct',
        'Alderaan' => 'Air Alderaan',
        'Tatooine' => 'Tatooine Transit'
    ];
    
    $shorts = [
        'Endor Express' => 'EE',
        'Bespin Direct' => 'BD',
        'Air Alderaan' => 'AA',
        'Tatooine Transit' => 'TT'
    ];
    
    $spacecrafts = [
        'Starspeeder 1000',
        'Starspeeder 3000'
    ];
    
    $priceoffset = 20;
    
    $flights = [];
    
    for ($i = 0; $i < $generation; $i++){
        foreach($spacelines as $origin => $spaceline){
            foreach($spacelines as $destination => $x){

                if ($origin == $destination){
                    continue;
                }
                
                $spaceline = $spacelines[$origin];
                if (random_int(0, 1)){
                    $spaceline = $spacelines[$destination];
                }
                
                $data = [
                    'origin' => $origin,
                    'destination' => $destination,
                    'flightno' => $shorts[$spaceline].random_int(100, 999),
                    'price' => $pricebase + random_int(-$priceoffset, $priceoffset),
                    'departure' => '2019-07-27',
                    'spaceline' => $spaceline,
                    'spacecraft' => $spacecrafts[random_int(0, 1)],
                    'duration' => random_int(60, 90)
                ];

                array_push($flights, $data);
            }
        }
    }
    
    
    $sql = "INSERT INTO flights VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    $stmt = $pdo->prepare($sql);
    foreach($flights as $data){
        try {
            $stmt->execute([
                $data['flightno'],
                $data['price'],
                $data['spaceline'],
                $data['departure'],
                $data['duration'],
                $data['origin'],
                $data['destination'],
                $data['spacecraft']
            ]); 
        } catch (PDOException $e){
            echo '<pre>';
            echo $e->getMessage();
            echo '<br><br>';
            echo '</pre>';
        }
    }
    debug($flights);
}

